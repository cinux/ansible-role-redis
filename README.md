redis
=========

Install and configure the in-memory Database redis.

Requirements
------------

none

Role Variables
--------------

You can define additional configurations for redis via the `cinux_redis_config_additional` as below:

```
cinux_redis_config_additional:
  redis.session.locking_enabled: 1
  redis.session.lock_retries: -1
  redis.session.lock_wait_time: 10000
```

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: cinux.redis}

License
-------

MIT